## Nadrox site, based on angular2-seed

Using Angular2 to build the new nadrox website

[![Build Status](https://semaphoreci.com/api/v1/paquettea/nadrox-2016/branches/master/badge.svg)](https://semaphoreci.com/paquettea/nadrox-2016)

### Usage
- Clone or fork this repository
- Make sure you have [node.js](https://nodejs.org/) installed
- run `npm install` to install dependencies
- run `npm start` to fire up dev server
- open browser to `http://localhost:8080`