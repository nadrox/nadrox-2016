///<reference path="../node_modules/angular2/typings/browser.d.ts"/>
import {enableProdMode} from 'angular2/core';
import {bootstrap} from 'angular2/platform/browser';
import {HTTP_PROVIDERS} from 'angular2/http';
import {ProfilesService} from './app/services/ProfilesService'

import {Nadrox} from './app/nadrox';

if (window.location.hostname.indexOf('nadrox.com') !== -1){
  enableProdMode()
}

bootstrap(Nadrox, [HTTP_PROVIDERS, ProfilesService])
  .catch(err => console.error(err));
