import {Component} from 'angular2/core';
import {Home} from './components/home/home';

@Component({
  selector: 'nadrox',
  providers: [],
  templateUrl: 'app/nadrox.html',
  styleUrls: ['vendors/sanitize.css'],
  directives: [Home],
  pipes: []
})

export class Nadrox {
  //not using date pipe in template because of https://github.com/angular/angular/issues/3333
  currentYear: Number = new Date().getFullYear();
  constructor() {}

}
