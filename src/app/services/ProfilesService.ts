import {Injectable} from 'angular2/core';
import {Http, Response} from 'angular2/http';
import {Observable} from 'rxjs/Observable';

import 'rxjs/add/operator/share';


@Injectable()
export class ProfilesService {

  private profiles$:Observable<Array<any>>;
  private profiles:Array<any>;
  private profilesObserver;

  constructor(private http:Http) {
    this.profiles$ = new Observable(observer => this.profilesObserver = observer).share();

    this.http
      .get('/assets/app/components/profiles/assets/profiles.json')
      .subscribe((res:Response) => {
        this.profiles = res.json();
        this.profilesObserver.next(this.profiles)
      });
  }

  getProfiles() {
    return this.profiles$;
  }

  setProfileExpanded(profile, isExpanded) {
    for (let _profile of this.profiles) {

      if (_profile === profile) {
        //if we find the profile, set the new value
        _profile.isExpanded = isExpanded;
      } else if (isExpanded) {
        //if it's another profile and that we are looking to expand one, collapse the others
        _profile.isExpanded = false;
      }

    }
    //publshes the new value;
    this.profilesObserver.next(this.profiles);
  }
}
