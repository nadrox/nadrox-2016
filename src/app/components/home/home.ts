import {Component} from 'angular2/core';
import {ProfileList} from '../profiles/profile-list';
@Component({
  selector: 'home',
  templateUrl: 'app/components/home/home.html',
  styleUrls: ['app/components/home/home.css'],
  providers: [],
  directives: [ProfileList],
  pipes: []
})
export class Home {

  constructor() {}

}
