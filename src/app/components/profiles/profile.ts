import {Component, Input, ElementRef, AfterViewChecked, ChangeDetectorRef} from 'angular2/core';
import {ProfilesService} from '../../services/ProfilesService';

@Component({
  selector: 'profile',
  templateUrl: 'app/components/profiles/profile.html',
  styleUrls: ['app/components/profiles/profile.css', 'app/components/profiles/assets/icons/nadrox-icons-embedded.css'],
  host: {
    '[class.expanded]': 'profile.isExpanded'
  }
})

export class Profile implements AfterViewChecked{
  @Input() profile;
  toggleProfile:Function;
  detailsHeight:Number = 0;
  el:ElementRef;
  profilesService: ProfilesService;
  cdRef: ChangeDetectorRef;

  constructor(profilesService:ProfilesService, el:ElementRef, cdRef:ChangeDetectorRef) {
    this.el = el;
    this.profilesService = profilesService;
    this.cdRef = cdRef;

    this.toggleProfile = function () {
      profilesService.setProfileExpanded(this.profile, !this.profile.isExpanded);
    };

  }

  ngAfterViewChecked(){
    this.updateHeight();
    this.cdRef.detectChanges();
  }

  private updateHeight() {
    if (this.profile && this.el.nativeElement) {

      if (this.profile.isExpanded) {
        this.detailsHeight = this.el.nativeElement.querySelector('.hidden-wrapper .details').offsetHeight;
      } else {
        this.detailsHeight = 0;
      }
    }
  }

}
