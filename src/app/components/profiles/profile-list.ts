import {Component} from 'angular2/core';
import {ProfilesService} from '../../services/ProfilesService';
import {Profile} from './profile';


@Component({
  selector: 'profile-list',
  templateUrl: 'app/components/profiles/profile-list.html',
  styleUrls: ['app/components/profiles/profile-list.css'],
  directives:[Profile]
})

export class ProfileList {
  profiles: any;
  constructor(private profilesService:ProfilesService) {
    profilesService.getProfiles().subscribe(profiles => this.profiles = profiles);
  }
}
